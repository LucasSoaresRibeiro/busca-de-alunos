﻿using System.IO;
using System.Collections.Generic;

using Newtonsoft.Json;
using System;

namespace BuscaDeAlunos.Models
{
    public class Student
    {
        public static string StudentFile = AppDomain.CurrentDomain.BaseDirectory + "\\App_Data\\Students.json";

        public long RA { get; set; }
        public DateTime SearchDate { get; set; }
        public string FormattedSearchDate { get; set; }
    }
}