﻿var map;
var googleApiMapKey = 'AIzaSyAsrcj7OColofVBkQHQOJDL0_dIQxGjyjY';
var tableId = '16301484656389751053-01619059540675406410';
var RaField = 'RA_Aluno';
var marker;
var infowindow;

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: { lat: -23.1918393, lng: -45.868433 },
        zoom: 8,
        mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
            position: google.maps.ControlPosition.LEFT_BOTTOM
        },
    });
}

function showError(errorMessage) {

    $("#modalErrorWindow .modal-body").html("<p>" + errorMessage + "</p>");
    $('#modalErrorWindow').modal('show');

}

function search() {

    var inputValue = $('#input_search').val();
    var query = RaField + '=' + inputValue;

    $.ajax({
        type: 'GET',
        async: false,
        dataType: "json",
        url: 'https://www.googleapis.com/mapsengine/v1/tables/' + tableId + '/features?where=' + query + '&version=published&key=' + googleApiMapKey,
        success: function (data) {

            try {

                if (data.features.length == 0) {
                    showError("Não foi encontrado o aluno com o RA " + inputValue);
                    return;
                }

                var firstFeature = data.features[0];
                var lat = firstFeature.geometry.coordinates[1];
                var lng = firstFeature.geometry.coordinates[0];

                createPin(lat, lng, firstFeature.properties.Endereco);
                saveResult(inputValue);

            } catch (e) {
                showError("Ops, algo deu errado por aqui!\nDetalhes:" + e.message);
            }
        },
        statusCode: {
            400: function (e) {
                showError("Ops, algo deu errado por aqui!\nDetalhes:" + e);
            }
        }
    });

}

function clearMarkers() {

    if (marker != null && marker.infowindow != null) {
        marker.infoWindow.setMap(null);
        marker.infoWindow = null;
    }

    if (marker != null) {
        marker.setMap(null);
    }
}

function createPin(lat, lng, content) {

    clearMarkers();
    marker = new google.maps.Marker({
        position: {lat: lat, lng: lng},
        map: map,
        title: content
    });

    map.setZoom(17);
    map.panTo(marker.position);

    createInfo(marker, content);

}

function createInfo(marker, content) {

    var contentString = '<h4 id="firstHeading" class="firstHeading">' + content + '</h4>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    infowindow.open(map, marker);

}

function saveResult(ra) {

    $.ajax({
        type: 'GET',
        async: true,
        dataType: "json",
        url: '/Student/SaveResult/' + ra,
        success: function (data) {
            console.log(data);
        },
        statusCode: {
            400: function (e) {
                showError("Ops, algo deu errado por aqui!\nDetalhes:" + e.message);
            }
        }
    });

}

function getSearchHistory() {

    $.ajax({
        type: 'GET',
        async: false,
        dataType: "json",
        url: '/Student/GetSearchHistory',
        success: function (data) {

            var columnA = 'RA';
            var columnB = 'FormattedSearchDate';

            // Create table result
            var htmlTable = '<table class="table table-hover">';
            htmlTable += '\n';

            // Header
            htmlTable += '<thead>';
            htmlTable += '\n';
            htmlTable += '<tr>';
            htmlTable += '\n';
            htmlTable += '<th>RA</th>';
            htmlTable += '\n';
            htmlTable += '<th>Data da Consulta</th>';
            htmlTable += '\n';
            htmlTable += '</tr>';
            htmlTable += '\n';
            htmlTable += '</thead>';
            htmlTable += '\n';

            // Body
            htmlTable += '<tbody>';
            htmlTable += '\n';
            for (var value in data) {

                htmlTable += '<tr>';
                htmlTable += '\n';

                htmlTable += '<td>' + data[value][columnA] + '</td>';
                htmlTable += '\n';

                htmlTable += '<td>' + data[value][columnB] + '</td>';
                htmlTable += '\n';

                htmlTable += '</tr>';
                htmlTable += '\n';
            }

            htmlTable += '</tbody>';
            htmlTable += '\n';
            htmlTable += '</table>';

            // Update html
            $("#modalWindow .modal-body").html(htmlTable);

            // Show modal
            $('#modalWindow').modal('show');
            
        },
        statusCode: {
            400: function (e) {
                showError("Ops, algo deu errado por aqui!\nDetalhes:" + e.message);
            }
        }
    });
}


function exportHistory() {

    window.open("/Student/ExportHistory");

}