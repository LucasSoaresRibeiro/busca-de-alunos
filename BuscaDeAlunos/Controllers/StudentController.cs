﻿using BuscaDeAlunos.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Linq;
using System.IO;

namespace BuscaDeAlunos.Controllers
{
    public class StudentController : Controller
    {
        private static List<Student> GetStudents()
        {
            List<Student> students = new List<Student>();
            if (System.IO.File.Exists(Student.StudentFile))
            {
                // File exists..
                string content = System.IO.File.ReadAllText(Student.StudentFile);
                // Deserialize the objects 
                students = JsonConvert.DeserializeObject<List<Student>>(content);

                // Returns the clients, either empty list or containing the Client(s).
                return students;
            }
            else
            {
                // Create the file 
                System.IO.File.Create(Student.StudentFile).Close();
                // Write data to it; [] means an array, 
                // List<Client> would throw error if [] is not wrapping text
                System.IO.File.WriteAllText(Student.StudentFile, "[]");

                // Re run the function
                GetStudents();
            }

            return students;
        }

        private static void SaveStudents(List<Student> StudentList)
        {
            System.IO.File.WriteAllText(Student.StudentFile, JsonConvert.SerializeObject(StudentList));
        }

        public string SaveResult(long ra)
        {
            List<Student> StudentList = GetStudents();

            Student student = new Student();
            student.RA = Convert.ToInt64(ra);
            student.SearchDate = DateTime.Now;
            student.FormattedSearchDate = student.SearchDate.ToString("G");
            StudentList.Add(student);

            // Sort by date and get first 10
            StudentList = StudentList.OrderByDescending(x => x.SearchDate).Take(10).ToList();

            SaveStudents(StudentList);

            return "Consulta salva com sucesso";
        }

        public string GetSearchHistory()
        {
            List<Student> StudentList = GetStudents();
            return JsonConvert.SerializeObject(StudentList);
        }

        public FileResult ExportHistory()
        {
            string studentsFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Student.StudentFile);
            byte[] fileBytes = System.IO.File.ReadAllBytes(studentsFile);
            string fileName = "Alunos.txt";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }
    }
}