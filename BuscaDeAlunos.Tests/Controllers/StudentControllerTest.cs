﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BuscaDeAlunos.Controllers;
using System.Web.Mvc;

namespace BuscaDeAlunos.Tests.Controllers
{
    [TestClass]
    public class StudentControllerTest
    {
        [TestMethod]
        public void SaveResultTest()
        {
            // Arrange
            StudentController controller = new StudentController();

            // Act
            string result = controller.SaveResult(123456) as string;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual("Consulta salva com sucesso", result);
        }

        [TestMethod]
        public void GetSearchHistoryTest()
        {
            // Arrange
            StudentController controller = new StudentController();

            // Act
            string result = controller.GetSearchHistory() as string;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ExportHistoryTest()
        {
            // Arrange
            StudentController controller = new StudentController();

            // Act
            FileResult result = controller.ExportHistory() as FileResult;

            // Assert
            Assert.IsNotNull(result);
        }

    }
}
