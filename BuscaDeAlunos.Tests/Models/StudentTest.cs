﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using BuscaDeAlunos.Models;
using System.Collections.Generic;
using System;

namespace BuscaDeAlunos.Tests.Models
{
    [TestClass]
    public class StudentTest
    {
        [TestMethod]
        public void CreateStudent()
        {
            var SearchDate = DateTime.Now;
            var FormattedSearchDate = SearchDate.ToString("G");

            // Create student
            Student student = new Student();
            student.RA = 123456789;
            student.SearchDate = SearchDate;
            student.FormattedSearchDate = FormattedSearchDate;

            // Assert values
            Assert.IsNotNull(student);
            Assert.AreEqual(123456789, student.RA);
            Assert.AreEqual(SearchDate, student.SearchDate);
            Assert.AreEqual(FormattedSearchDate, student.FormattedSearchDate);
        }
    }
}
